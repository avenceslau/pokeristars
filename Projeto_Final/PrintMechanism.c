//
//  PrintMechanism.c
//  Projeto_Final
//
//  Created by André Venceslau, Alexandre Lopes on 06/04/2020.
//  Copyright © 2020 André Venceslau, Alexandre Lopes. All rights reserved.
//

#include <stdio.h>
#include <string.h>

#include "PrintMechanism.h"
#include "auxiliary_func.h"

//Itera as cartas do valor + baixo para o valor + alto
//combo size determina o número de cartas iguais
void PrintOneCombo(char player_cards[][3], int number_of_cards, int card_value[], int score){
    int i, number_of_repetitions = 0;
    int counter = 1;
    int position_tmp = 4;
    int card = 0;
    char tmp_hand[7][3];
    
    if (score == 2 ) { //1 Par
        number_of_repetitions = 2;
    } else if (score == 4){ //1 Trio
        number_of_repetitions = 3;
    } else if (score == 8) {// Poker
        number_of_repetitions = 4;
    }
    
    for (i = number_of_cards - 2; i >= 0 ; i--) {
        if (card_value[i] == card_value[i + 1]) {
            counter++;
            if (counter == number_of_repetitions) {
                card = card_value[i];
                break;
            }
        } else if(counter != number_of_repetitions){
            counter = 1;
        }
    }
    
    for (i = number_of_cards - 1 ; i >= 0 ; i--) {
        if (card_value[i] == card) {
            //Print das cartas presentes no conjunto
            strcpy(tmp_hand[position_tmp], player_cards[i]);
            WriteCharOutFile("%s ", tmp_hand[position_tmp]);
            position_tmp--;
        } else if (counter < 5){
            //Print das restantes cartas, começando na carta de valor + alto, não é feito print das cartas pertencentes à combinação
            strcpy(tmp_hand[position_tmp], player_cards[i]);
            WriteCharOutFile("%s ", tmp_hand[position_tmp]);
            counter++;
            position_tmp--;
        }
    }
    CleanNFillSpecial(player_cards, tmp_hand);
}

//Encontra os 2 melhores conjuntos e faz print
//Adicionando uma última melhor carta se for 2 pares (score = 3)
void PrintTwoCombo(char player_cards[][3], int number_of_cards, int cardvalue[], int score){
    //Guarda valores das duas combinações, combinação de pontuação + alta card[1] e combinação de pontuação + baixa card[0]
    int counter = 1, i;
    int card[2] = {0, 0};
    int card_i = 1; //Posição no array card[]//Posição no array card[]
    int position_tmp = 4;
    char tmp_hand[7][3];
    
    for (i = number_of_cards - 2 ; i >= 0 ; i--) {
        if (cardvalue[i] == cardvalue[i + 1]) {
                counter++;
                
                //Combo na última posição
                if (i == 0){
                    //Fullhouse
                    if (score == 7) {
                        if ((counter == 3) && (card[1] == 0)) {
                            card[1] = cardvalue[i+1];
                            counter = 1;
                        }
                        //Guarda 2º Trio
                        else if ((counter == 3) && (card[0] == 0)) {
                            card[0] = cardvalue[i+1];
                            counter = 1;
                        }
                        //Guarda Par + alto
                        else if ((counter == 2) && (card[0] == 0)){
                            card[0] = cardvalue[i];
                            counter = 1;
                        }
                    }
                    //2 Par
                    else if (card[0] == 0 && score == 3){
                        card[card_i] = cardvalue[i];
                    }
                }
            }
            
            //Combo em qualquer outra posição
            if (cardvalue[i] != cardvalue[i+1]) {
                //Fullhouse
                if (score == 7) {
                    //Guarda Trio
                    if ((counter == 3) && (card[1] == 0)) {
                        card[1] = cardvalue[i+1];
                    } else if ((counter == 3) && (card[0] == 0)){
                        card[0] = cardvalue[i+1];
                    }
                    //Guarda Par + alto
                    else if ((counter == 2) && (card[0] == 0)){
                        card[0] = cardvalue[i+1];
                    }
                }
                    //2 Pares
                    else if (score == 3){
                        if (counter == 2){
                            card[card_i] = cardvalue[i+1];
                            card_i--;
                            
                            if (card_i == -1) {
                                break;
                            }
                        }
                    }
                counter = 1;
                }
    }
    
    //2 Pares
    if (score == 3){
        for (i = number_of_cards - 1, counter = 0 ; i >= 0 ; i--) {
            if ((cardvalue[i] == card[1]) || (cardvalue[i] == card[0])) {
                strcpy(tmp_hand[position_tmp], player_cards[i]);
                WriteCharOutFile("%s ", tmp_hand[position_tmp]);
                position_tmp--;
            }
            else if(counter < 1) {
                strcpy(tmp_hand[position_tmp], player_cards[i]);
                WriteCharOutFile("%s ", tmp_hand[position_tmp]);
                counter++;
                position_tmp--;
            }
        }
    }
    //Fullhouse
    else if (score == 7) {
        for (i = number_of_cards - 1, counter = 0; i >= 0 ; i--) {
            if ((cardvalue[i] == card[0]) || (cardvalue[i] == card[1])) {
                strcpy(tmp_hand[position_tmp], player_cards[i]);
                WriteCharOutFile("%s ", tmp_hand[position_tmp]);
                counter++;
                position_tmp--;
            }
            if (counter >= 5) {
                break;
            }
        }
    }
    CleanNFillSpecial(player_cards, tmp_hand);
}

void StraightPrint(char straight[][3]){
    int i;
    int num_rep = 0; //Número de cartas iguais no straight
    int counter = 0;
    char tmp_straight[7][3];
    int position = 4;
    
    for (i = ArraySize(straight) - 2; i >= 0; i--) {
        if (strncmp(straight[i], straight[i+1], 1) != 0) {
            strcpy(tmp_straight[position], straight[i+1+num_rep]);
            WriteCharOutFile("%s ", tmp_straight[position]);
            counter++;
            position--;
            num_rep = 0;
            if (counter == 5) {
                break;
            }
            if ((i == 0) && (counter < 5)) {
                strcpy(tmp_straight[position], straight[i+num_rep]);
                WriteCharOutFile("%s ", tmp_straight[position]);
                position--;
            }
        } else if ((strncmp(straight[i], straight[i+1], 1) ==  0) && (i == 0)){
            strcpy(tmp_straight[position], straight[i+1+num_rep]);
            WriteCharOutFile("%s ", tmp_straight[position]);
            position--;
        } else if(counter == 5){
            break;
        } else {
            num_rep++;
        }
    }
    CleanNFillSpecial(straight, tmp_straight);
}


void FlushesPrint(char cards[][3]){
    int i;
    char tmp_flush[7][3];
    int counter = 0;
    int position = 4;
    
    for (i = ArraySize(cards) - 1; i >= 0; i--) {
        strcpy(tmp_flush[position], cards[i]);
        WriteCharOutFile("%s ", tmp_flush[position]);
        counter++;
        position--;
        if (counter == 5) {
            break;
        }
    }
    CleanNFillSpecial(cards, tmp_flush);
}

void HighcardPrint(char  player_cards[][3], int number_of_cards){
    int i, counter = 0;
    char tmp_hand[7][3];
    int position = 4;
    
    for (i = number_of_cards - 1 ; i >= 0 ; i--) {
        strcpy(tmp_hand[position], player_cards[i]);
        WriteCharOutFile("%s ", tmp_hand[position]);
        counter++;
        position--;
        if (counter == 5) {
            break;
        }
    }
    CleanNFillSpecial(player_cards, tmp_hand);
}

//Faz print da melhor mão com base no score
void BestHandPrint(char player_cards[][3], int number_of_cards, char flush_cards[][3], char straight[][3], char straight_flush_cards[][3], int cardvalue[], int score){
    
    switch (score) {
        //Print Highcard
        case 1:
            HighcardPrint(player_cards, number_of_cards);
            break;
            
        case 2: //Print Par
        case 4: //Print Trio
        case 8: //Print Poker
            PrintOneCombo(player_cards, number_of_cards, cardvalue, score);
            break;
            
        case 3:
        case 7:
            PrintTwoCombo(player_cards, number_of_cards, cardvalue, score);
            break;
        case 5:
            StraightPrint(straight);
            RegularCleanAndFill(player_cards, straight);
            break;
        case 6:
            FlushesPrint(flush_cards);
            RegularCleanAndFill(player_cards, flush_cards);
            break;
        case 9:
        case 10:
            FlushesPrint(straight_flush_cards);
            RegularCleanAndFill(player_cards, straight_flush_cards);
            break;
        default:
            break;
    }
}


