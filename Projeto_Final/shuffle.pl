#!/usr/bin/perl
use strict;
use warnings;

use File::Find qw(find);
use File::Slurp;

my $poker = './pokeristars ';
my $tests_dir = './shuffletests/';
my $tests_out = './out/';
my $debug = 0;
my @outs = ();


sub list_dir {
        my @dirs = @_;
        my @files;
        find({ wanted => sub { push @files, glob "\"$_/*.shuffle\"" } , no_chdir => 1 }, @dirs);
        return @files;
}    
    
my $cmd;
my $res;
my @files = list_dir($tests_dir);


`mkdir $tests_out`;
# for each decks file
foreach my $file (@files) {
	print "processando ficheiro $file ...\n";

	#
	print "Command: -s1\n";
	#

	# executa poker -comando deck
	my $out_file = $file;
	$out_file =~ s/\.shuffle/\.mixed/;
	$out_file =~ s/Ins/Outs/;
	print "out_file; $out_file\n" unless !$debug;
	my $cmd = "$poker -s1 $file";
	print "gonna do cmd $cmd\n" unless !$debug;
	$res = `$cmd`;
	
	print "didi cmd $cmd\n" unless !$debug;

	# grava resultado do poker
	my $out_poker_file = $file;
	$out_poker_file =~ s/.*?Ins\///;
	$out_poker_file =~ s/\.shuffle/\.mixed/;
	$out_poker_file = $tests_out.$out_poker_file;
	print "Should save poker output on $out_poker_file\n" unless !$debug;
	write_file("$out_poker_file",$res);

	# sdiff entre resultado poker e saida esperada
	my $diff_cmd = "sdiff $out_poker_file $out_file | egrep -n \"\\||<|>\"";
	print "diff_cmd: $diff_cmd\n" unless !$debug;
	my $diff_res = `$diff_cmd`;

	# FIXME save $diff_res to file
	my $diff_file = $out_file;
	$diff_file =~ s/.*?saidas\//$tests_out/;
	$diff_file =~ s/\.mixed/\.txt/;
	print "should save sdiff result into $diff_file\n" unless !$debug;
	write_file($diff_file, $diff_res);
	print "\n";
}

`rm -rf $tests_out`;
