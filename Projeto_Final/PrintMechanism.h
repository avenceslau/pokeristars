//
//  PrintMechanism.h
//  Projeto_Final
//
//  Created by André Venceslau, Alexandre Lopes on 06/04/2020.
//  Copyright © 2020 André Venceslau, Alexandre Lopes. All rights reserved.
//

#ifndef PrintMechanism_h
#define PrintMechanism_h

#include <stdio.h>

void BestHandPrint(char player_cards[][3], int number_of_cards, char flush_cards[][3], char straight[][3], char straight_flush_cards[][3], int cardvalue[], int score);

#endif /* PrintMechanism_h */
