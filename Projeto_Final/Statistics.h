//
//  Statistics.h
//  Projeto_Final
//
//  Created by André Venceslau, Alexandre Lopes on 04/04/2020.
//  Copyright © 2020 André Venceslau, Alexandre Lopes. All rights reserved.
//

#ifndef Statistics_h
#define Statistics_h

#include <stdio.h>
#include "DxAux.h"

void GenerateStats(void);

void StatsDataPoints(int score);
void StatsDataWhoWins(int winner);
void StatsPrintRatio(double ratio[], bool di);
void StatsPrintWinner(void);
void StatsCalculator(int ncards);
void StatsCalculatorDx(int ngames);
void StatsDx(player_node * head, bool who_points[PLAYER_NUM], int num_winners);
void PrintStatDx(player_node * head);

#endif /* Statistics_h */
