#!/usr/bin/perl
use strict;
use warnings;
use File::Find qw(find);
use File::Slurp;

my $poker = './pokeristars -c ';
my $tests_dir = './tests/';

sub list_dir {
        my @dirs = @_;
        my @files;
        find({ wanted => sub { push @files, glob "\"$_/*.hands\"" } , no_chdir => 1 }, @dirs);
        return @files;
}

my $cmd;
my $cards;
my $res;
my @files = list_dir($tests_dir);
my $erros = 0;
my $total = 0;

foreach my $file (@files) { 
	my @cards = read_file($file);
	$file =~ s/hands$/out/;
	my @results = read_file($file);

	for (my $i = 0; $i < scalar(@cards); $i++) {
		$cmd = $poker . $cards[$i];
		$res = `$cmd`;
		my $r1 = chomp($results[$i]);
		my $r2 = chomp($res);
		if ($r1 ne $r2) {
			print "Erro:\n";
			print "Dir: $file\n";
	       		print "cmd: $cmd";
			print "expected: \t" . ".$results[$i].";
			print "result: \t.$res.\n";
			$erros++;
		}
		else {
	#		print "SUCESSO\n";
	#		print "Dir: $file\n";
	#		print "cmd: $cmd";
	#		print "expected: \t." . $results[$i].".\n";
	#		print "result: \t.$res.\n";
		}
		$total++;
	}
}
 print "$erros erros em $total\n"

