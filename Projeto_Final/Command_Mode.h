//
//  Command_Mode.h
//  Projeto_Final
//
//  Created by André Venceslau, Alexandre Lopes on 29/04/2020.
//  Copyright © 2020 André Venceslau, Alexandre Lopes. All rights reserved.
//

#ifndef Command_Mode_h
#define Command_Mode_h

#include <stdio.h>

void SaveCards(int num_of_parameters, char * arr[], char cards[][3], int ncards);
void CModeSelection(int ncards, int num_of_parameters, char cards[][3]);
int FiveCards(int number_of_parameters, char player_x[][3]);
int SevenCards(int number_of_parameters, char arr[][3]);
int NineCards (int number_of_cards, char arr[][3]);
int TenCards(int number_of_parameters, char arr[][3]);

#endif /* Command_Mode_h */
