//
//  Shuffle_Mode.c
//  Projeto_Final
//
//  Created by André Venceslau, Alexandre Lopes on 28/04/2020.
//  Copyright © 2020 André Venceslau, Alexandre Lopes. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "Shuffle_Mode.h"
#include "auxiliary_func.h"

#define DECKSIZE 52
#define DECKHALF 26

typedef struct mix mix_node;

typedef struct deck card;

//Estrutura que guarda o modo de baralhar
struct mix{
    int how_to_shuffle; //shuffle mode 1,2,3
    mix_node * next;
    mix_node * previous;
};

//Estutura que guarda uma carta
struct deck{
    char Card[3];
    card * next;
    card * previous;
};

card * AddCard(card ** head, card ** tail){
    card * new;
    
    new = (card *) malloc(sizeof(card));
    if ((*head) == NULL) {
        *head = new;
        *tail = new;
        new->next = NULL;
        new->previous = NULL;
    } else {
        (*tail)->next = new;
        new->next = NULL;
        new->previous = *tail;
        *tail = new;
    }
    return new;
}

mix_node * NewShakeMix(mix_node ** head, mix_node ** tail){
    mix_node * new;
    new = (mix_node *) malloc(sizeof(mix_node));
    if (*head == NULL) {
        *head = new;
        *tail = new;
        new->next = NULL;
        new->previous = NULL;
    } else {
        (*tail)->next = new;
        new->next = NULL;
        new->previous = *tail;
        *tail = new;
    }
    return new;
}

//Recebe a head e a tail de um baralho
//Faz print do baralho e free da memória ocupada pelas várias cartas do baralho
void PrintDeck(card *head, card *tail){
    card * ptr;
    int j = 0;
    ptr = head;
    do {
        WriteCharOutFile("%s ", ptr->Card);
        free(ptr->previous);
        ptr = ptr->next;
        j++;
        if (j == 13) {
            WriteCharOutFile("%s", "\n");
            j = 0;
        }
    } while (ptr != NULL);
    free(tail);
}

//Recebe a head e a tail do baralho em questão. Implementa o modo de mistura 3
void NewDeckCut(card ** head, card ** tail){
    card * ptr_to_card = NULL;
    int i;
    
    ptr_to_card = (*head);
    //Encontra a carta do meio
    for(i = 0 ; i < DECKHALF ; i++){
        ptr_to_card = ptr_to_card->next; //No último i carta 27
    }
    //a tail passa a ser a carta do meio e aponta para a head
    (*tail)->next = (*head);
    (*head)->previous = (*tail);
    //colocamos a head e tail no sítio correto
    (*head) = ptr_to_card;
    (*tail) = ptr_to_card->previous;
    //limpar previous head/next tail
    (*head)->previous = NULL;
    (*tail)->next = NULL;
}



//Esta função percorre e baralha a lista das cartas, são baralhados as ligações da lista e não o conteúdo mesma
//Recebe-se o ínicio da lista e o final do Deck. Implementa o modo de mistura 1
void NewRiffleShuffle(card ** head, card ** tail){
    int i;
    card * runner; //percorre a lista do meio até à tail
    card * runner_half;//percorre a lista da head até ao meio da lista
    card * save_half_next; //guarda o next da primeira metade
    card * save_runner_next; //guarda o next da segunda metade
    
    runner_half = (*head);
    runner = (*head);
    for(i = 0 ; i < DECKHALF ; i++){
        runner = runner->next; //No último i carta 27
    }
    //Coloca as cartas aternadamente no deck pela nova ordem correta
    //Por cada ciclo coloca-se a uma carta da 2ªmetade entre cada uma das cartas da 1ª metade
    //Ex.: se o runner for AC, o runner->next for 2C, runner->previous for KO, o runner_half for AE e o runner_half->next for 2E então
    //runner_half->next = AC, runner->next = 2E, runner->previous->next(next do KO) = 2C, runner->previous = AC, 2C->previous = KO, runner->previous = AE, 2E->previous =AC
    do  {
        //são guardados os nexts
        save_half_next = runner_half->next;
        runner_half->next = runner;
        save_runner_next = runner->next;
        //são inseridos os novos nexts
        runner_half->next = runner;
        runner->next = save_half_next;
        runner->previous->next = save_runner_next;
        //são inseridos os novos previous
        save_runner_next->previous = runner->previous;
        runner->previous = runner_half;
        save_half_next->previous = runner;
        //preparação para o próximo ciclo
        runner_half = save_half_next;
        runner = save_runner_next;
    } while (runner != *tail);
}

//Esta função percorre e baralha a lista das cartas, são baralhados as ligações da lista e não o conteúdo mesma
//Recebe-se o ínicio da lista e o final do Deck. Implementa o modo de mistura 1
void SpecialRiffleShuffle(card ** head, card **tail) {
    card * runner; //variável que percorre a lista da head para tail
    card * save_tail; //guarda o endereço do previous do tail
    
    runner = *head;
    //Por cada ciclo coloca-se a carta do fim da lista entre o runner e o seu next
    //Ex.: se o runner for AC e o runner->next for 2C e o tail for KP então
    //tail->next 2C, tail->previous AC, runner->next->previous(previous do 2C) = tail, runner->next = tail
    do {
        (*tail)->next = runner->next;
        //Salvaguarda do previous do tail
        save_tail = (*tail)->previous;
        //colocação do novo previous da tail
        (*tail)->previous = runner;
        //como save tail irá ser a nova tail então o next será null última posição da lista
        save_tail->next = NULL;
        //coloca-se a tail entre next do runner e o runner
        runner->next->previous = *tail;
        runner->next = *tail;
        //Preparação do próximo ciclo
        runner = (*tail)->next;
        *tail = save_tail;
    } while (runner->next != *tail);
}


//Recebe como parâmetros o ínicio e o fim da lista com as cartas de um deck, e a maneira como irá baralhar
void Shake(card ** head, card ** tail,int operation){
    if (operation == 3) {
        NewDeckCut(head, tail);
    } else if (operation == 2){
        SpecialRiffleShuffle(head, tail);
    } else if (operation == 1){
        NewRiffleShuffle(head, tail);
    }
}

//Recebe apontadores para o primeiro e o último comandos, e para a primeira e última cartas de um deck
void LetsShake(mix_node * mix_head, mix_node * mix_tail, card ** head, card ** tail){
    mix_node * ptr;
    
    ptr = mix_head;
    do {
        Shake(head, tail, ptr->how_to_shuffle);
        free(ptr->previous);
        ptr = ptr->next;
    } while (ptr != NULL);
    free(mix_tail);
}

bool EasierToSee(int ch){
    return (ch != ' ') && (ch != '\n') && (ch != EOF) && (ch != '\t');
}


//Esta função recebe o apontador para o ficheiro. Percorre o ficheiro e para cada conjunto mistura/deck baralha-o
void AddBlend(FILE * file) {
    int j = 0; // //conta numero de cartas adicionadas a um deck
    char helper = ' ';
    int ch;
    card * new_card = NULL;
    card * card_head = NULL;
    card * card_tail = NULL;
    mix_node * mix_head = NULL;
    mix_node * mix_tail = NULL;
    mix_node * new_mix = NULL;
    
    
    //Este ciclo percorre o ficheiro e guarda nas listas corretas os dados associados às mesmas
    do {
        ch = fgetc(file);
        if (EasierToSee(ch)) {
            helper = ch;
            ch = fgetc(file);
        }
        //Se se encontar apenas um caractere seguido de um espaço/enter/tab então cria-se um mix node e guarda-se o número dentro desse node
        if (EasierToSee(helper) && ((ch == ' ') || (ch == '\n') || (ch == EOF) || (ch == '\t'))) {
            new_mix = NewShakeMix(&mix_head, &mix_tail);
            new_mix->how_to_shuffle = helper - '0';
            helper = ' '; //Reset helper
            
            //Se se encontrar dois carateres diferentes de espaço/enter/tab então sabemos que estamos na presença de um deck guardamos a primeira carta
        } else if (EasierToSee(ch) && EasierToSee(helper)) {
            new_card = AddCard(&card_head, &card_tail);
            //Adiciona primeira carta
            card_head->Card[0] = helper;
            card_head->Card[1] = ch;
            card_head->Card[2] = '\0';
            j++;
            //Encontramos as restantes 51 cartas do deck
            do {
                ch = fgetc(file);
                helper = ch;
                if (EasierToSee(helper)) {
                    ch = fgetc(file);
                    new_card = AddCard(&card_head, &card_tail);
                    new_card->Card[0] = helper;
                    new_card->Card[1] = ch;
                    new_card->Card[2] = '\0';
                    j++;
                }
            } while (j < DECKSIZE);
            LetsShake(mix_head, mix_tail, &card_head, &card_tail);
            PrintDeck(card_head, card_tail);
            WriteCharOutFile("%s", "\n");
            //reset dos pointer para a mistura, do helper, e da variável j
            j = 0;
            helper = ' ';
            card_head = NULL;
            card_tail = NULL;
            mix_head = NULL;
            mix_tail = NULL;
        }
    }while (ch != EOF);
    CloseOutFile();
}


void ShakeMode(char * arguments[], int number_arguments){
    
    FILE * file = NULL;
    
    file = GetFile(arguments[2]);
    AddBlend(file);
    fclose(file);
}
