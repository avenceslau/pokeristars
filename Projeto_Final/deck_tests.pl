#!/usr/bin/perl
use strict;
use warnings;
use File::Find qw(find);
use File::Slurp;

my $poker = './pokeristars ';
my $tests_dir = './tests_dx/';
my $tests_out = './out/';
my $debug = 0;
my @outs = (
	"d1", 
	"d2", 
	"d3", 
	"d4",
	"dx"
);

sub list_dir {
        my @dirs = @_;
        my @files;
        find({ wanted => sub { push @files, glob "\"$_/*.deck\"" } , no_chdir => 1 }, @dirs);
        return @files;
}

my $cmd;
my $res;
my @files = list_dir($tests_dir);

# for each decks file
`mkdir $tests_out`;
foreach my $file (@files) { 
	print "processando baralho $file ...\n";

	#FIXME ler cada um dos comandos 
	foreach my $i (@outs) {
		

		#FIXME?
		#
		#
		print "Command: -$i\n";
		#
		#

		# executa poker -comando deck
		my $out_file = $file;
		$out_file =~ s/\.deck/_$i\.out/;
		$out_file =~ s/baralhos/saidas/;
		my $cmd = "$poker -$i $file";
		$res = `$cmd`;

		# grava resultado do poker
		my $out_poker_file = $file;
		$out_poker_file =~ s/.*?baralhos\///;
		$out_poker_file =~ s/\.deck/_$i.out/;
		$out_poker_file = $tests_out.$out_poker_file;
		print "should saver poker output on $out_poker_file\n" unless !$debug;
		write_file("$out_poker_file",$res);
	
		# sdiff entre resultado poker e saida esperada
		my $diff_cmd = "sdiff $out_poker_file $out_file | egrep -n \"\\||<|>\"";
		print "diff_cmd: $diff_cmd\n" unless !$debug;
		my $diff_res = `$diff_cmd`;
		print "diff is $diff_res\n";

		# save $diff_res to file
		my $diff_file = $out_file;
		$diff_file =~ s/.*?saidas\//$tests_out/;
		$diff_file =~ s/\.out/\.txt/;

		print "should save sdiff result into $diff_file\n" unless !$debug;
		write_file($diff_file, $diff_res);
	}
}

`rm -rf $tests_out`;

