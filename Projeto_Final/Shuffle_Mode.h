//
//  Shuffle_Mode.h
//  Projeto_Final
//
//  Created by André Venceslau, Alexandre Lopes on 28/04/2020.
//  Copyright © 2020 André Venceslau, Alexandre Lopes. All rights reserved.
//

#ifndef Shuffle_Mode_h
#define Shuffle_Mode_h

#include <stdio.h>

void ShakeMode(char * arguments[], int number_arguments);

#endif /* Shuffle_Mode_h */
