//
//  Pointing_System.c
//  Projeto_Final
//
//  Created by André Venceslau, Alexandre Lopes on 04/04/2020.
//  Copyright © 2020 André Venceslau, Alexandre Lopes. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "Pointing_System.h"
#include "auxiliary_func.h"


#define UMPAR 0x1
#define DOISPAR 0x2
#define TRESPAR 0x3
#define UMTRIO 0x4
#define DOISTRIO 0x5
#define UMPARUMTRIO 0x6
#define DOISPARUMTRIO 0x7
#define POKER 0x8

//Calcula o número de Áses numa mão
int HowManyAces(int card_value[], int number_of_parameters){
    int i;
    int position = 0;
    for (i = number_of_parameters-1 ; i >= 2 ; i--) {
        if (card_value[i] == 14) {
            position++;
        } else{
            return position;
        }
    }
    return 0;
}

//Esta altera a ordem das strings para que os Áses vão para o ínicio e altera o valor númerico dos A de 14 para 1
void AceInverter(char arr[][3], int card_value_source[], int number_of_parameters, int card_value_dest[],char inverted_array[][3], int number_of_aces){
    int i;
    int num_aces = 0;
    //inverte a posição dos Áses de uma mão
    for (i = number_of_parameters-1 ; i >= 0 ; i--) {
        if (card_value_source[i] == 14) {
            strcpy(inverted_array[num_aces], arr[i]);
            card_value_dest[num_aces] = 1;
            num_aces++;
        }
    }
    SuitOrder(inverted_array, num_aces, card_value_dest);
    //coloca restantes cartas na mão
    for (i = 0, num_aces = number_of_aces ; i < (number_of_parameters - number_of_aces) ; i++ , num_aces++) {
        card_value_dest[num_aces] = card_value_source[i];
        strcpy(inverted_array[num_aces], arr[i]);
    }
}


//Calcula o número de cartas envolidas num straight que comece por A sendo que pode ser uma sequência de 7 cartas, as cartas que podem ser straight são guardadas no array straight_cards
//Ex.: Se a mão for 2C, 3C , 4C, 5C, 5E, 6E, AC, o array straight_cards[i] terá AC, AE, 2C, 3C, 4C, 5C, 6E
//Isto é feito para que noutra função possa ser confirmada a existência do straight flush que neste caso seria AC, 2C, 3C, 4C, 5C
int StartingWithAceStraight(int number_of_parameters, char hand[][3], int card_value[], char straight_cards[][3]){
    int i, num_of_aces;
    int card_i = 0;
    int repetitions = 1;
    int counter = 1; //Conta o número de cartas diferentes que podem estar envolvidas num potencial straight do exemplo anterior contaria 6, um dos A e as restantes cartas
    int card_value_tmp[number_of_parameters];
    char array_tmp[number_of_parameters+1][3];
   
    
    for (i = 0 ; i < number_of_parameters; i++) {
        card_value_tmp[i] = card_value[i];
    }
    
    
    num_of_aces = HowManyAces(card_value, number_of_parameters);
    if (num_of_aces > 0) {
        AceInverter(hand, card_value, number_of_parameters, card_value_tmp, array_tmp, num_of_aces);
        for (i = 1 ; i < number_of_parameters ; i++) {
                if (card_value_tmp[i] - 1 == card_value_tmp[i-1]){
                    counter++;
                    repetitions++;
                    if (repetitions == 2) {
                        strcpy(straight_cards[card_i],array_tmp[i-1]);
                        card_i++;
                        strcpy(straight_cards[card_i], array_tmp[i]);
                        card_i++;
                    } else {
                        strcpy(straight_cards[card_i], array_tmp[i]);
                        card_i++;
                    }
                } else if(card_value_tmp[i] == card_value_tmp[i-1]){
                    repetitions++;
                    if (repetitions == 2) {
                        strcpy(straight_cards[card_i],array_tmp[i-1]);
                        card_i++;
                        strcpy(straight_cards[card_i], array_tmp[i]);
                        card_i++;
                    } else {
                        strcpy(straight_cards[card_i], array_tmp[i]);
                        card_i++;
                    }
                }
                else {
                    if (counter < 5) {
                        counter = 1;
                        card_i = 0;
                        repetitions = 1;
                        ArrayCleaner(straight_cards, number_of_parameters);
                    }
                    if (counter >= 5) {
                        break;
                    }
                }
            }
    }
    if (counter >= 5) {
        straight_cards[card_i][0] = '\0';
        return counter;
    } else {
        card_i = 0;
        ArrayCleaner(straight_cards, number_of_parameters);
        return 0;
    }
}



//Calcula o número de cartas envolidas num straight que não comece por A sendo que pode ser uma sequência de 7 cartas, as cartas que podem ser straight são guardadas no array straight_cards
//Ex.: Se a mão for 2C, 3C , 4C, 5C, 5E, 6E, 7C, o array straight_cards[i] terá 2C, 3C, 4C, 5C, 5E, 6C, 7E
//Isto é feito para que noutra função possa ser confirmada a existência do straight flush que neste caso seria 2C, 3C, 4C, 5C, 6C
int NotStartingWithAceStraight(int number_of_parameters, char arr[][3], int card_value[], char straight_cards[][3]){
    int i, card_i = 0;
    int counter = 1; //Conta o número de cartas diferentes que podem estar envolvidas num potencial straight do exemplo anterior contaria 6, um dos 5 e as restantes cartas
    int repetitions = 1;
    
    for (i = 1 ; i < number_of_parameters ; i++) {
        if (card_value[i] - 1 == card_value[i-1]){
            counter++;
            repetitions++;
            if (repetitions == 2) {
                strcpy(straight_cards[card_i],arr[i-1]);
                card_i++;
                strcpy(straight_cards[card_i], arr[i]);
                card_i++;
            } else {
                strcpy(straight_cards[card_i], arr[i]);
                card_i++;
            }
        } else if(card_value[i] == card_value[i-1]){
            repetitions++;
            if (repetitions == 2) {
                strcpy(straight_cards[card_i],arr[i-1]);
                card_i++;
                strcpy(straight_cards[card_i], arr[i]);
                card_i++;
            } else {
                strcpy(straight_cards[card_i], arr[i]);
                card_i++;
            }
        }
        else {
            if (counter < 5) {
                counter = 1;
                card_i = 0;
                repetitions = 1;
                ArrayCleaner(straight_cards, number_of_parameters);
            }
            if (counter >= 5) {
                break;
            }
        }
    }
    
    if (counter < 5) {
        card_i = 0;
        ArrayCleaner(straight_cards, number_of_parameters);
    }
    if (counter >= 5) {
        straight_cards[card_i][0] = '\0';
        return counter;
    } else {
        return 0;
    }
    
}


//Esta função recebe o tamanho de ambos os casos dos straights, e para  o caso de um potencial straight flush escolhe
int FatherStraight(int number_of_parameters, char arr[][3], int card_value[], char straight[][3]){
    char normal_straight[number_of_parameters+1][3];
    char starting_with_ace_straight[number_of_parameters+1][3];
    int num_cards_straight; //Número de cartas diferentes, num straight não começado por A
    int num_cards_straight_wA; //Número de cartas diferentes, num straight começado por A
    
    num_cards_straight = NotStartingWithAceStraight(number_of_parameters, arr, card_value, normal_straight);
    num_cards_straight_wA = StartingWithAceStraight(number_of_parameters, arr, card_value, starting_with_ace_straight);
    
    //verifica-se em qual dos straights existem mais cartas para o caso de haver uma sequência de cartas do A ao 7
    //Se houver uma sequência do A ao 7 então o A deve ser inclúido para que o caso de haver um straight flush
    if (num_cards_straight > num_cards_straight_wA) { //Se não começar por A então há sempre mais cartas envolvidas num_cards_straight, o outro é zero
        ArrayFiller(straight, normal_straight);
        return 5; //Pontuação do straight
    } else if((num_cards_straight_wA >= num_cards_straight) && (num_cards_straight_wA != 0)){ //Se num_cards_straight_wA > 0 quer dizer que o straight começa por A logo tem sempre + 1 do que num_cards_straight
        ArrayFiller(straight, starting_with_ace_straight);
        return 5; //Pontuação do straight
    } else {
        return 0;
    }
}



//Verifica se existe um flush, e guarda em flush_cards as várias cartas pertencentes ao flush caso ele exista
int Flush(int number_of_parameters, char arr[][3],int card_value[], char flush_cards[][3]){
    int i;
    int position = 0;
    char suit;
    int diamonds_counter = 0, clubs_counter = 0, spades_counter = 0, hearts_counter = 0;
    
    for (i = 0 ; i < number_of_parameters ; i++) {  //Conta para cada naipe quantas cartas existem em cada um
        if (strchr(arr[i], 'C') != NULL){
            hearts_counter++;
        }
        else if (strchr(arr[i], 'E') != NULL){
            spades_counter++;
        }
        else if (strchr(arr[i], 'P') != NULL){
            clubs_counter++;
        }
        else if (strchr(arr[i], 'O') != NULL){
            diamonds_counter++;
        }
    }
    
    //Se houver um flush é indicado qual o naipe das cartas pertencentes a esse flush
    if (hearts_counter >= 5) {
        suit = 'C';
    } else if(spades_counter >= 5){
        suit = 'E';
    } else if(clubs_counter >= 5){
        suit = 'P';
    } else if(diamonds_counter >= 5){
        suit = 'O';
    } else {
        return 0;
    }
    
    //São colucados nas posições corretas as cartas envolvidas no flush
    for (i = 0; i < number_of_parameters ; i++) {
        if (strchr(arr[i], suit) != NULL) {
            strcpy(flush_cards[position], arr[i]);
            position++;
        }
    }
    flush_cards[position][0] = '\0';
    return 6; //Pontuação do flush
}


//Nesta função verifica-se a existência de um straight flush ou um royal flush
//Em straight_flush_cards[] são guardadas as cartas que fizerem parte de um straight flush ou de um royal flush
int StraightAndRoyalFlush(char flush_cards[][3], char straight_flush_cards[][3]){
    int evaluator= 0;
    int flush_len = ArraySize(flush_cards);
    int cardvalue[flush_len];
    
    GiveCardValue(flush_cards, cardvalue);
    //Verifica se o flush tb é straight
    evaluator = FatherStraight(flush_len, flush_cards, cardvalue, straight_flush_cards);
    
    if (evaluator != 0){
        if (strchr(straight_flush_cards[ArraySize(straight_flush_cards)- 1] , 'A') != NULL) { //Se a última carta for um A então é royal flush
            return 10; //Pontuação de um royal flush
        } else{ //Se não tiver A no final é um straight flush
            return 9; //Pontuação de um straight flush
        }
    }
    
    return 0;
}



//Esta função descodifica a bit_mask e em função do resultado retorna o resultado correspondente à pontuação das cartas envolvidas
unsigned int DecoderBitMask(unsigned int bit_mask){
    switch (bit_mask) {
        
        case 1:
            return 2;
            break;
        case 2:
        case 3:
            return 3;
            break;
        case 4:
            return 4;
            break;
        case 5:
        case 6:
        case 7:
            return 7;
            break;
        case 8:
            return 8;
        default:
            return 0;
            break;
    }
}



//Esta função verifica se existem pares, trios, pokeres ou qualquer combinação dos três
unsigned int TwoThreeOrFour(int ncards, char card[][3]){
    char previous_card = '\0';
    int counter = 1;
    int res[3] = { 0, 0, 0};
    int res_i  = 0;
    int i;
    unsigned int bit_mask = 0x0;

    // itera as cartas
    for (i = 0 ; i < ncards ; i++){
        // conta as cartas repetidas
        if (card[i][0] == previous_card) {
            counter++;
            if (i == ncards - 1) { res[res_i] = counter; }
        }
        // carta diferente, guarda o valor do counter e faz reset
        else {
            if (counter > 1) {
                res[res_i] = counter;
                res_i++;
            }
            previous_card = card[i][0];
            counter = 1;
        }
    }
    //As somas permitem saber quais os conjuntos de cartas envolvidas, por exemplo se existir um poker e um par, por esta ordem, então, res[0] = 4(Poker) e res[1] = 2, sendo assim a soma 6
    
    if (res[0] + res[1] + res[2] == 2) {bit_mask = UMPAR;}
    else if (res[0] + res[1] + res[2] == 3) {bit_mask = UMTRIO; }
    else if (res[0] + res[1] + res[2] == 5) {bit_mask = UMPARUMTRIO; }
    else if (res[0] + res[1] + res[2] == 6) {
        if((res[0] == 4) || (res[1] == 4)){
            bit_mask = POKER;
        } else if (res[0] == 2) {
        bit_mask = TRESPAR;
        } else {
            bit_mask = DOISTRIO;
        }
    }
    else if (res[0] + res[1] + res[2] == 4) {
        if (res[0] == 2 || res[1] == 2) {
            bit_mask = DOISPAR;
        } else {
            bit_mask = POKER;
        }
    }
    else if (res[0] + res[1] + res[2] == 7) {
        if (res[0] + res[1] == 7) {
            bit_mask = POKER;
        } else {
            bit_mask =  DOISPARUMTRIO;
        }
    }
    return bit_mask;
}


//Esta função faz o stitching de todas as funções envolvidas na pontuação de uma jogada, é retornada a variável score com a pontuação
int PointCalc(char player_cards[][3], int number_of_parmeters, char flush_cards[][3], char straight[][3], char straight_flush_cards[][3], int cardvalue[]){
    int score = 0;
    int flush_points = 0, straight_points = 0, straight_flush_points = 0, combo_points = 0;
    
    //Pontuação referente ao flush
    flush_points = Flush(number_of_parmeters, player_cards, cardvalue, flush_cards);
    //Pontuação referente ao straight
    straight_points = FatherStraight(number_of_parmeters, player_cards, cardvalue, straight);
    
    //Se não houver flush ou se não houver straight não é verificado se existe straightflush
    if ((flush_points != 0) && (straight_points != 0)) {
        straight_flush_points = StraightAndRoyalFlush(flush_cards, straight_flush_cards);

        if (straight_flush_points != 0) {
            return straight_flush_points;
        }
        
    }
    
    //Pontuação referente às combinações de pares, trios e pokeres
    combo_points = DecoderBitMask(TwoThreeOrFour(number_of_parmeters, player_cards));
    
    //Verifica-se qual a maior pontuação para mão introduzida
    score = MaxOfThreeNumbers(combo_points, flush_points, straight_points);
    
    if (score == 0) {
        return 1; //Highcard
    }
    return score;
}
