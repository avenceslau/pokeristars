//
//  DxAux.h
//  Projeto_Final
//
//  Created by André Venceslau, Alexandre Lopes on 07/05/2020.
//  Copyright © 2020 André Venceslau, Alexandre Lopes. All rights reserved.
//

#ifndef DxAux_h
#define DxAux_h

#include <stdio.h>
#include "DeckMode.h"

#define PLAYER_NUM 8

typedef struct player player_node;

struct player{
    double num_wins; //pontuação total
    player_node * next; //pointer para o próximo jogador
    player_node * previous; //pointer para o jogador anterior
    int player_id; //id do jogador
    int consecutive_plays; //número de jogos seguidos
    int num_folds; //número de folds seguidos
    int score; //score do jogo atual
    char hand[3][3]; //mão do jogador ( 2 cartas)
    char best_hand[6][3]; //melhor mão do jogador ( combinação da mão e da mesa)
    bool forced_play; //booleano que define se o jogador é obrigado a jogar
    bool cooldown; //booleano que define se o jogador está em cooldown
};

int CheckForRepCards(int num_of_parameters, char cards[][3]);
void Initializer(player_node * ptr);
void CooldownReset(player_node * ptr_to_player);
void PutCardsToHand(char hand[7][3], char exclusive_player_cards[2][3], char table[5][3]);
void DoIPlay(player_node * player_x);

void ResetPlayerScores(bool who_points[PLAYER_NUM]);
void ResetRound(player_node * player_x);

void GetMinScore(int player_id);
int PlayerID(void);
int MinScore(void);
void GetPlayerId(int player_id);

#endif /* DxAux_h */
