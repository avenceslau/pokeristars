//
//  Unmatch.c
//  Projeto_Final
//
//  Created by André Venceslau, Alexandre Lopes on 06/04/2020.
//  Copyright © 2020 André Venceslau, Alexandre Lopes. All rights reserved.
//

#include <stdio.h>

#include "Unmatch.h"
#include "auxiliary_func.h"
#include "DxAux.h"
#include "Statistics.h"

//Indica qual dos 2 jogadores tem maior pontuação
int TwoPlayersWhoScores(int a, int b){
    if (a > b){
        return 1;
    } else if (b > a) {
        return 2;
    } else {
        return 0;
    }
}


int HighestCard(int number_of_parameters, int card_value_1[], int card_value_2[]){
    int i;
    int winner = 0;
    
    //começando na carta mais alta, testa até à de menor valor
    for(i = number_of_parameters - 1 ; i >= 0 ; i--){
        //se a carta em i do jogador 1 for maior do que a do jogador 2, o jogador 1 ganha, retornando 1
        if(card_value_1[i] > card_value_2[i]){
            return 1;
        //inverso do comentário anterior
        } else if (card_value_2[i] > card_value_1[i]){
            return 2;
        // se a carta mais baixa for igual, enão todas as cartas de ambos os jogadores têm o mesmo valor, não havendo vencedor
        } else {
            winner = 0;
        }
    }
    
    //Se ambas as mãos forem iguais retorna 0;
    return winner;
}


//Encontra uma única combinação de cartas (Par, Trio, Poker)
int FindCombination(int number_of_parameters, char player_1[][3], char player_2[][3], int score){
    int i, n = 0;
    int counter_player_1 = 0, counter_player_2 = 0;
    int cards_values_player_1[5], cards_values_player_2[5];
    int card[2] = {0, 0}; // card[0] repetida player 1, card[1] repetida player 2
    
    GiveCardValue(player_1, cards_values_player_1);
    GiveCardValue(player_2, cards_values_player_2);
    
    //Apenas precisa de encontrar a 1ª carta repetida pois se o score for 2 ou 4 sabemos que apenas pode existir uma combinação de cartas
    if (score == 2 || score == 4) {
        n = 1;
    }
    
    //Precisa de encontrar todas as cartas pertencentes ao poker pois pode haver uma outra combinação Ex.: Um par e um poker
    if (score == 8){
        n = 3;
    }
    
    for (i = number_of_parameters-1; i >= 1; i--) {
        //Encontra a combinação do player 1
        if (cards_values_player_1[i] == cards_values_player_1[i-1]) {
            counter_player_1++;
            if (counter_player_1 == n) {
                card[0] = cards_values_player_1[i];
            }
            
        }
        //Encontra a combinação do player 2
        if (cards_values_player_2[i] == cards_values_player_2[i-1]) {
                        counter_player_2++;
                        if (counter_player_2 == n) {
                            card[1] = cards_values_player_2[i];
                        }
            
        }
    }
    //Verifica-se qual das combinações vale mais e se forem iguais faz se o highest card entre os 2 jogadores
    if (card[0] > card[1]) {
        return 1;
    } else if (card[1] > card[0]){
        return 2;
    } else if (card[0] == card[1]) {
        return HighestCard(5, cards_values_player_1, cards_values_player_2);
    }
    return 0;
}


//Encontra 2 combinações
int FindTwoCombinations(int number_of_parameters, char player_1[][3], char player_2[][3], int score){
    int i, winner = 0;
    int card_i_player_1 = 0, card_i_player_2 = 0; // card_i ajuda com a posição no caso dos dois pares
    int combinations_player_1[2] = {0, 0};// comb[0] e comb[1] são valores de pares, par maior [0], par menor [1], trio ou par que não interessa comb[2] player 1
    int combinations_player_2[2] = {0, 0};// comb[0] e comb[1] são valores de pares, par maior [0], par menor [1], trio ou par que não interessa comb[2] player 2
    int counter_player_1 = 0;
    int counter_player_2 = 0;
    int cards_values_player_1[5], cards_values_player_2[5];
    
    GiveCardValue(player_1, cards_values_player_1);
    GiveCardValue(player_2, cards_values_player_2);
    
    //Descobre as cartas repetidas de cada jogador e guarda-as no sítio correto
    for (i = number_of_parameters - 2 ; i >= 0; i--) {
        //player 1
        MultipleCombosPlayerX(cards_values_player_1, &counter_player_1, combinations_player_1, i, &card_i_player_1);
        //player 2
        MultipleCombosPlayerX(cards_values_player_2, &counter_player_2, combinations_player_2, i, &card_i_player_2);
    }
    
    //2 ou 3 pares
    if (score == 3) {
        winner = TwoPlayersWhoScores(combinations_player_1[0], combinations_player_2[0]); //Par mais alto
        if (winner == 0) {
            winner = TwoPlayersWhoScores(combinations_player_1[1], combinations_player_2[1]); //Par mais baixo
            if (winner == 0) {
                winner = HighestCard(5, cards_values_player_1, cards_values_player_2);
            }
        }
    }
    
    //Fullhouse
    if (score == 7) {
        winner = TwoPlayersWhoScores(combinations_player_1[1], combinations_player_2[1]); //Trio mais alto
        if (winner == 0) {
            winner = TwoPlayersWhoScores(combinations_player_1[0], combinations_player_2[0]); //Par mais baixo
            if (winner == 0) {
                winner = HighestCard(5, cards_values_player_1, cards_values_player_2);
            }
        }
    }
    return winner;
}

//Vê qual das sequências Flush e straight é maior
int UnmatchSequence(char player_1_sequence[][3], char player_2_sequence[][3]){
    int winner = 0;
    int card_values_1[5], card_values_2[5];
      
    GiveCardValue(player_1_sequence, card_values_1);
    GiveCardValue(player_2_sequence, card_values_2);
    winner = HighestCard(5, card_values_1, card_values_2);
    
    return winner;
}

//Chama as diferentes funções com base no score
unsigned int Unmatching(int score, int number_of_parameters, char player_1[][3], char player_2[][3]){
    int winner = 0;
    int cards_player_1_values[5], cards_player_2_values[5];
    
    switch (score) {
        case 1:
            GiveCardValue(player_1, cards_player_1_values);
            GiveCardValue(player_2, cards_player_2_values);
            winner = HighestCard(5, cards_player_1_values, cards_player_2_values);
            break;
        case 2:
        case 4:
        case 8:
            winner = FindCombination(number_of_parameters, player_1, player_2, score);
            break;
        case 3:
        case 7:
            winner = FindTwoCombinations(number_of_parameters, player_1, player_2, score);
            break;
        case 10:
            winner = 0;
            break;
        case 5:
            winner = UnmatchSequence(player_1, player_2);
            break;
        case 6:
            winner = UnmatchSequence(player_1, player_2);
            break;
        case 9:
            winner = UnmatchSequence(player_1, player_2);

            break;
        default:
            break;
    }
    return winner;
}

//Verifica qual dos jogadores ganha
int WhoWins(int score_player_1, int score_player_2, int number_of_parameters_per_player, char player_1[][3], char player_2[][3]){
    int winner;
    int score;
    
    if(score_player_1 > score_player_2){
        winner = 1;
    } else if(score_player_2 > score_player_1){
        winner = 2;
    //Se o score for igual então é feito o desempate
    } else {
        score = score_player_1;
        winner = Unmatching(score, number_of_parameters_per_player, player_1, player_2);
    }
    
    return winner;
}

bool UnmatchDx(player_node * head){
    int best_score = 0;
    int index, i;
    int winner = 0;
    bool who_points[PLAYER_NUM];
    unsigned int num_of_winners = 0;
    player_node * ptr_to_player = NULL;
    player_node * ptr_best_player = NULL;
    ResetPlayerScores(who_points);
    ptr_to_player = head;
    do {
        index = ptr_to_player->player_id - 1;
        best_score = MaxOfTwoNumbers(ptr_to_player->score, best_score);
        if (best_score == ptr_to_player->score) {
            if (ptr_best_player == NULL) {
                who_points[index] = true;
                num_of_winners++;
                ptr_best_player = ptr_to_player;
            } else {
                winner = WhoWins(ptr_best_player->score, ptr_to_player->score, 5, ptr_best_player->best_hand, ptr_to_player->best_hand);
                if (winner == 2) {
                    ResetPlayerScores(who_points);
                    num_of_winners = 0;
                    who_points[index] = true;
                    num_of_winners++;
                    ptr_best_player = ptr_to_player;
                } else if (winner == 0){
                    who_points[index] = true;
                    num_of_winners++;
                }
            }
        }
        ptr_to_player = ptr_to_player->next;
    } while (ptr_to_player != NULL);
    ptr_best_player->best_hand[5][0] = '\0';
    if (best_score != 0) {
        if (num_of_winners != 0) {
            StatsDataPoints(best_score);
            WriteCharOutFile("%s", "W ");
            for (i = 4; i >= 0 ; i--) {
                WriteCharOutFile("%s ", ptr_best_player->best_hand[i]);
            }
            WriteIntOutFile("%d", best_score);
            for (i = 0 ; i < PLAYER_NUM ; i++) {
                if (who_points[i] == true) {
                    WriteIntOutFile(" %d", i+1);
                }
            }
            WriteCharOutFile("%s", "\n\n");
            ptr_best_player = NULL;
        }
        StatsDx(head, who_points, num_of_winners);
        return true;
    } else {
        return false;
    }
}


