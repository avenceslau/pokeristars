//
//  auxiliary_func.c
//  Projeto_Final
//
//  Created by André Venceslau, Alexandre Lopes on 04/04/2020.
//  Copyright © 2020 André Venceslau, Alexandre Lopes. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "auxiliary_func.h"
#define NUM_PLAYERS 8
#define BASEINDEX 600 //Este offset faz com que não existam carateres ascii que enrem em conflito com o cardchecker

//guardam as coisas relacionadas com o modo -o, pointer para o ficheiro, o nome do ficheiro, e um bool proceed que diz se o modo -o foi
FILE * outfile;
char * outfilename;
bool proceed;

int GetCardId(char cards[3]){
    int key = 0;
    switch (cards[1]) {
        case 'C':
        case 'E':
        case 'P':
        case 'O':
            key += BASEINDEX; //Se o naipe for o correto então soma-se
            break;
        default:
            return 0;
            break;
    }
    switch (cards[0]) {
        case 'T':
        case 'J':
        case 'Q':
        case 'K':
        case 'A':
            key += 2;
            break;
        default:
            key += cards[0]-'0';
            break;
    }
    return key;
}

//Recebe como parâmetro um card_id e indica se a carta é válida
bool CheckCardId(int card_id){
    if (card_id >= BASEINDEX+2) { //602
        if (card_id <= BASEINDEX+9) { //602
            return true;
        }
    }
    return false;
}

//Recebe como parâmetros o número de cartas a verificar, e as cartas
//Ou faz return do número de cartas enontradas ncards, ou -1 no caso de encontrar uma carta inválida
int BetterCardChecker(int number_of_cards, char card[][3]){
    int i;
    int ncards = 0;
    
    for (i = 0; i < number_of_cards; i++) {
        if (CheckCardId(GetCardId(card[i])) == true) {
            ncards++;
        } //else {
//            GetOut();
//        }
    }
    if (number_of_cards == ncards) {
        return ncards;
    }
    return -1;
}

//Guarda as combinações de um jogador, usada em tandem com a função FindTwoCombinations
void MultipleCombosPlayerX(int card_values_player_x[], int *counter_player_x, int combo_player_x[2], int i, int *card_i_player_x){
    //se a carta em que se encontra for igual à próxima então aumenta o contador,
    if(card_values_player_x[i] == card_values_player_x[i+1]){
        (*counter_player_x)++;
        //se i = 0 e a carta atual for igual às anteriores então é necessário guardar o seu valor
        if (i == 0){
            if (*counter_player_x == 1) {
                combo_player_x[*card_i_player_x] = card_values_player_x[i+1];
            } else if (*counter_player_x == 2){
                combo_player_x[1] = card_values_player_x[i+1];
            }
        }
    }
    //se o counter for diferente de zero então guardamos o valor da carta que ainda fazia parte das combinações
    else {
        if ((*counter_player_x) == 1) {
            combo_player_x[*card_i_player_x] = card_values_player_x[i+1];
            (*card_i_player_x)++;
            (*counter_player_x) = 0;
        }
        if (*counter_player_x == 2) {
            combo_player_x[1] = card_values_player_x[i+1];
            (*counter_player_x) = 0;
        }
    }
}

//Verifica se existem comandos inválidos
void CheckForInvalidCommands(int num_of_parameters, char * arguments[]){
    char * acceptable_commands[8] = {"-c","-d1","-d2","-d3","-d4","-dx","-s1","-o"};
    int i, j;
        
    for (i = 2; i < num_of_parameters ; i++) {
        for (j = 0 ; j < 8; j++) {
            //Verifica se existem comandos para além do comando -c, se existirem faz-se get out
            if (strcmp(arguments[2], "-c") == 0) {
                if (strcmp(arguments[i], acceptable_commands[j]) == 0){
                    GetOut();
                }
            //Nos outros modos verifica-se se existem outros comandos para além do -o nesse caso GetOut
            } else {
                if (strcmp(arguments[i], acceptable_commands[j]) == 0){
                    if ( strcmp(arguments[i], "-o") != 0) {
                        GetOut();
                    }
                }
            }
        }
    }
}

//Verifica se o ficheiro existe
void CheckFile(FILE * ptr){
    if (ptr == NULL) {
        GetOut();
    }
}

//Verifica se existe um ficheiro com a extensão correta
void CheckFileExtension(char * argument_to_check, char * file_extension){
    char * tmp;
    tmp = strstr(argument_to_check, file_extension);
    if (tmp  == NULL) {
        GetOut();
    } else {
        if (strcmp(tmp,file_extension) != 0) {
            GetOut();
        }
    }
    
}

//Abre um ficheiro com nome file_name
FILE * GetFile(char * file_name){
    FILE * file = NULL;
    file = fopen(file_name, "r");
    CheckFile(file);
    return file;
}

//Mensagem de erro e saída do programa
void GetOut(void){
    printf("-1\n");
    exit(0);
}

//Limpa um array de strings de 3 caratéres que tenha \0 no final
void ArrayCleaner(char arr[][3], int number_of_parameters){
    int i;
    for(i = 0 ; i<number_of_parameters ; i++){
        arr[i][0] = '\0';
    }
}

//Calcula o tamanho de um array de strings que tenha \0 no final
int ArraySize(char array[][3]){
    int i = 0, len = 0;
    
    for (i = 0; ; i++) {
        if (array[i][0] == '\0'){
            break;
        } else {
                len++;
        }
    }
    return len;
}


//Põe os componentes de um array de strings de 3 caratéres  que tenha \0 no último membro dentro de outro de tamanho semelhante, pondo '\0' no final do array de destino
void ArrayFiller(char dest_array[][3], char source_array[][3]){
    int i, counter;
    
    counter = ArraySize(source_array);
    for (i = 0; i < counter ; i++) {
        strcpy(dest_array[i],source_array[i]);
    }
    dest_array[counter][0] = '\0';
}

//Troca dois inteiros
void SwapInt(int * xp, int * yp){
    int tmp;
    tmp = * xp;
    * xp = * yp;
    * yp = tmp;
}

//Troca dois doubles
void SwapDouble(double * xp, double * yp){
    double tmp;
    tmp = * xp;
    * xp = * yp;
    * yp = tmp;
}

//Troca duas strings
void SwapStr(char * xp, char * yp){
    char tmp[3];
    strcpy(tmp, xp);
    strcpy(xp, yp);
    strcpy(yp, tmp);
   
}

//Coloca por ordem alfabética de naipes as cartas de igual valor numérico
void SuitOrder(char card[][3], int number_of_cards, int card_value[]){
    int i,j;
    for (i = 0; i < number_of_cards - 1; i++) {
        for (j = 0 ; j < number_of_cards - 1 ; j++) {
            if ((card_value[j] == card_value[j+1]) && (card[j][1] < card[j+1][1])) {
                SwapStr(card[j], card[j+1]);
            }
        }
    }
}

//Atribui a cada carta o valor numérico correspondente, é retornado o cardvalue
void GiveCardValue(char cards[][3], int card_value[]){
    int i;
    int number_of_cards;
    char * ptr ;

    number_of_cards = ArraySize(cards);
    for (i = 0 ; i < number_of_cards ; i++) {
        switch (cards[i][0]) {
            case 'T': card_value[i] = 10; break;
            case 'J': card_value[i] = 11; break;
            case 'Q': card_value[i] = 12; break;
            case 'K': card_value[i] = 13; break;
            case 'A': card_value[i] = 14; break;
            default:
                card_value[i] = (int) strtol(cards[i], &ptr, 10);
                break;
        }
    }
}

//Organiza as cartas por valor numérico,e de seguida por naipe em cartas do mesmo valor
void BubbleSort(int num_parameters, char cards[][3], int card_value[]){
    int i, j;
    char * ptr ;
    for (i = 0 ; i < num_parameters ; i++) {
        switch (cards[i][0]) {
            case 'T': card_value[i] = 10; break;
            case 'J': card_value[i] = 11; break;
            case 'Q': card_value[i] = 12; break;
            case 'K': card_value[i] = 13; break;
            case 'A': card_value[i] = 14; break;
            default:
                card_value[i] = (int) strtol(cards[i], &ptr, 10);
                break;
        }
        
    }
    for (i = 0; i < num_parameters - 1; i++) {
        for (j = 0 ; j < num_parameters - i - 1 ; j++) {
            if(card_value[j] > card_value[j+1]){
                SwapInt(&card_value[j], &card_value[j+1]);
                SwapStr(cards[j], cards[j+1]);
            }
            if ((card_value[j] == card_value[j+1]) && (cards[j][1] < cards[j+1][1])) {
                SwapStr(cards[j], cards[j+1]);
            }
        }
    }
}

//Verifica qual o maior de três números, devolve o maior
int MaxOfThreeNumbers(int a, int b, int c){
    int max;
    
    max = (a > b) ? a : b;
    return (max > c) ? max : c;
}

//Devolve o maior de 2 inteiros
int MaxOfTwoNumbers(int a, int b){
    return (a > b) ? a : b;
}

//Recebe como argumento o nome do ficheiro de saída
//Cria um ficheiro de sáida com o nome recebido
void OutMode(char * filename){
    proceed = true;
    outfilename = filename;
    outfile = fopen(outfilename, "a+");
}

//Escreve no ficheiro de saída
//Recebe em what to write algo como "%s" e em variable to write algo como "Escreve isto" ou um char * var, sendo var = "Variável 1"
void WriteCharOutFile(char * what_to_write, void * variable_to_write){
    if (proceed == true) {
        fprintf(outfile, what_to_write, variable_to_write);
    } else {
        printf(what_to_write, variable_to_write);
    }
}

//Escreve no ficheiro de saída
//Seguindo o exemplo anterior what_to_write será algo como %d e variable_to_write será uma variável do tipo int
void WriteIntOutFile(char * what_to_write, int variable_to_write){
    if (proceed == true) {
        fprintf(outfile, what_to_write, variable_to_write);
    } else {
        printf(what_to_write, variable_to_write);
    }
}

//Escreve no ficheiro de saída
//Seguindo o exemplo anterior what_to_write será algo como %lf e variable_to_write será uma variável do tipo double
void WriteDoubleOutFile(char * what_to_write, double variable_to_write){
    if (proceed == true) {
        fprintf(outfile, what_to_write, variable_to_write);
    } else {
        printf(what_to_write, variable_to_write);
    }
}

//Fecha o ficheiro de saída
void CloseOutFile(){
    if (proceed == true) {
        fclose(outfile);
    }
}

//Faz clean do 1º parametro, e faz fill com o 2º, não existe necessidade souce[5][0] = '\0' serve para que n existam promblemas ao usar o array fill, porque destination já tem '\0'
void RegularCleanAndFill(char destination[][3], char source[][3]){
    ArrayCleaner(destination, ArraySize(destination));
    ArrayFiller(destination, source);
}

//Faz clean do 1º parametro, e faz fill com o 2º, source[5][0] = '\0' serve para que n existam promblemas ao usar o array fill
void CleanNFillSpecial(char destination[][3], char source[][3]){
    ArrayCleaner(destination, ArraySize(destination));
    source[5][0] = '\0';
    ArrayFiller(destination, source);
}

//Coloca por ordem da sua pontuação os id dos jogadores da maior para a mais baixa
void PlayerBubbleSort(int ID[],double num_wins[]){
    int i,j;

    for (i = 0; i < NUM_PLAYERS - 1; i++) {
        for (j = 0 ; j < NUM_PLAYERS - i - 1 ; j++) {
            if(num_wins[j] > num_wins[j+1]){
                SwapDouble(&num_wins[j], &num_wins[j+1]);
                SwapInt(&ID[j], &ID[j+1]);
            }
            else if(num_wins[j] == num_wins[j+1]){
                if(ID[j] < ID[j+1]){
                    SwapDouble(&num_wins[j], &num_wins[j+1]);
                    SwapInt(&ID[j], &ID[j+1]);
                }
            }
        }
    }
}


