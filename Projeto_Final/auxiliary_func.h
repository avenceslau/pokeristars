//
//  auxiliary_func.h
//  Projeto_Final
//
//  Created by André Venceslau, Alexandre Lopes on 04/04/2020.
//  Copyright © 2020 André Venceslau, Alexandre Lopes. All rights reserved.
//

#ifndef auxiliary_func_h
#define auxiliary_func_h

#include <stdio.h>

void CheckForInvalidCommands(int num_of_parameters, char * arr[]);
int BetterCardChecker(int number_of_cards, char card[][3]);

void ArrayCleaner(char arr[][3], int number_of_parameters);
void ArrayFiller(char dest_array[][3], char source_array[][3]);
int ArraySize(char array[][3]);
void RegularCleanAndFill(char destination[][3], char source[][3]);
void CleanNFillSpecial(char destination[][3], char source[][3]);

void SuitOrder(char card[][3], int number_of_cards, int card_value[]);
void GiveCardValue(char cards[][3], int card_value[]);
void BubbleSort(int num_parameters, char arr[][3], int card_value[]);
void MultipleCombosPlayerX(int card_values_player_x[], int *counter_player_x, int combo_player_x[2], int i, int *card_i_player_x);

int MaxOfThreeNumbers(int a, int b, int c);
int MaxOfTwoNumbers(int a, int b);

void GetOut(void);

FILE * GetFile(char * file_name);
void CheckFileExtension(char * argument_to_check, char * file_extension);

void OutMode(char * arguments);
//void OpenOutFile(void);
void WriteCharOutFile(char * what_to_write, void * variable_to_write);
void WriteIntOutFile(char * what_to_write, int variable_to_write);
void WriteDoubleOutFile(char * what_to_write, double variable_to_write);
void CloseOutFile(void);

void PlayerBubbleSort(int ID[],double num_wins[]);

#endif /* auxiliary_func_h */

