//
//  Pointing_System.h
//  Projeto_Final
//
//  Created by André Venceslau, Alexandre Lopes on 04/04/2020.
//  Copyright © 2020 André Venceslau, Alexandre Lopes. All rights reserved.
//

#ifndef Pointing_System_h
#define Pointing_System_h


int PointCalc(char player_cards[][3], int number_of_parmeters, char flush_cards[][3], char straight[][3], char straight_flush_cards[][3], int cardvalue[]);

#endif /* Pointing_System_h */
