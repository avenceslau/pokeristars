//
//  main.c
//  Projeto_Final
//
//  Created by André Venceslau, Alexandre Lopes on 04/04/2020.
//  Copyright © 2020 André Venceslau, Alexandre Lopes. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "auxiliary_func.h"
#include "Unmatch.h"
#include "DeckMode.h"
#include "Command_Mode.h"
#include "Shuffle_Mode.h"

//Função que implementa -c
//Recebe o número de pârametros e os argumentos argc e argv
void CMode(int number_of_parameters, char * arguments[]){
    char cards[number_of_parameters-1][3]; //Número de cartas = nº paramametros-2 no entanto é necessário +1 posição para que as funções de manipulação de arrays funcionem
    int ncards;
    
    cards[number_of_parameters-2][0] = '\0'; //Garante que as funções de manipulação de arrays funcionem
    CheckForInvalidCommands(number_of_parameters, arguments);
    SaveCards(number_of_parameters, arguments, cards, number_of_parameters-2);
    ncards = BetterCardChecker(number_of_parameters - 2,cards);
    
    CModeSelection(ncards, number_of_parameters, cards);
}

//Função que implementa o comando -di e -dx
//Recebe o número de pârametros e os argumentos argc e argv
void DMode(int number_of_parameters, char * arguments[]){
    CheckForInvalidCommands(number_of_parameters, arguments);
    CheckFileExtension(arguments[2], ".deck");
    DModeSelection(arguments);
}

//Funçao que implementa -s1
//Recebe o número de pârametros e os argumentos argc e argv
void SMode(int number_of_parameters, char * arguments[]){
    CheckForInvalidCommands(number_of_parameters, arguments);
    CheckFileExtension(arguments[2], ".shuffle");
    ShakeMode(arguments, number_of_parameters);
}

//Verifica se o comando -o está na posição certa
void OMode(int number_of_parameters, char *arguments[]){
    if (number_of_parameters == 5) {
        if (strcmp("-o", arguments[3]) == 0) {
            OutMode(arguments[4]);
        } else {
            GetOut();
        }
    }
}

//Verifica qual o modo de funcionamento do programa
void CommandCheck(int number_of_parameters, char *arguments[]){
    if (number_of_parameters == 1) {
        GetOut();
    } else if(strcmp("-c",arguments[1]) == 0 ){
        CMode(number_of_parameters, arguments);
    } else if(strncmp("-d", arguments[1], 2) == 0){
        if ((number_of_parameters == 3) || (number_of_parameters == 5)) {
            OMode(number_of_parameters, arguments);
            DMode(number_of_parameters, arguments);
        } else {
            GetOut();
        }
    } else if (strncmp("-s1", arguments[1], 2) == 0) {
        if ((number_of_parameters == 3) || (number_of_parameters == 5)) {
            OMode(number_of_parameters, arguments);
            SMode(number_of_parameters, arguments);
        } else {
            GetOut();
        }
    } else {
        GetOut();
    }
}

int main(int argc, char *argv[]){
    CommandCheck(argc, argv);
    return 0;
}
