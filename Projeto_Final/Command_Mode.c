//
//  Command_Mode.c
//  Projeto_Final
//
//  Created by André Venceslau, Alexandre Lopes on 29/04/2020.
//  Copyright © 2020 André Venceslau, Alexandre Lopes. All rights reserved.
//

#include <stdio.h>
#include <string.h>

#include "Command_Mode.h"
#include "auxiliary_func.h"
#include "Pointing_System.h"
#include "Unmatch.h"
#include "PrintMechanism.h"
#include "Statistics.h"
#include "DxAux.h"

//Copia as cartas dos argumentos para um vetor cartas
void SaveCards(int num_of_parameters, char * arguments[], char cards[][3], int ncards){
    int i,j;
    
    //Condição especial para caso de serem dez cartas
    if (ncards == 10) {
        for (i = 2; i < 6 ; i++) {
            for (j = i + 1 ; j < 7 ; j++) {
                // se houver repeticoes na primeira mao, sai com -1
                if (strcmp(arguments[i], arguments[j]) == 0) {
                    GetOut();
                }
                // se houver repeticoes na segunda mao, sai com -1
                if (strcmp(arguments[i+5], arguments[j+5]) == 0) {
                    GetOut();
                }
            }
        }
    }
    //Todos os outros casos
    else {
        //Verifica se existem cartas repetidas na mão
        for (i = 2; i < num_of_parameters - 1 ; i++) {
            for (j = i + 1 ; j < num_of_parameters ; j++) {
                if (strcmp(arguments[i], arguments[j]) == 0) {
                    GetOut();
                } else if (arguments[i][2] != '\0'){
                    GetOut();
                }
            }
        }
    }
    
    for (i = 0 ; i < ncards ; i++) {
        strcpy(cards[i], arguments[i + 2]);
    }
}

//Seleciona o modo de funcionamento do comando -c com base no número de cartas
void CModeSelection(int ncards, int num_of_parameters, char cards[][3]){
    int score = 0;
    //Quando o número de argumentos é x, o número de cartas introduzidas é x-2 = ncards
    switch(ncards){
        case 5:
            //Garante que não foram introduzidos argumentos inválidos se forem 5 cartas então tem de ser 7 elementos
            if (num_of_parameters == 7) {
                score = FiveCards(ncards, cards);
                printf("%d\n", score);
            } else {
                GetOut();
            }
            
            break;
        case 7:
            if (num_of_parameters == 9) {
                score = SevenCards(ncards, cards);
                printf("%d\n", score);
            } else {
                GetOut();
            }
            break;
        case 9:
            if (num_of_parameters == 11) {
                score = NineCards(ncards, cards);
                printf("%d\n", score);
            } else {
                GetOut();
            }
            break;
        case 10:
            if (num_of_parameters == 12) {
                score = TenCards(ncards, cards);
                printf("%d\n", score);
            } else {
                GetOut();
            }
            break;
            
        default:
            GetOut();
    }
}


//Recebe 5 cartas de um jogador faz return do score
int FiveCards(int number_of_parameters, char player_x[][3]){
    int card_numeric_value[number_of_parameters];
    int score;
    char flush_cards[7][3]; //Guarda as cartas que podem fazer parte de um flush
    char straight_cards[7][3]; //Guarda as cartas que podem fazer parte de um straight
    char straight_flush_cards[7][3]; //Guarda as cartas que podem fazer parte de um straight flush
    
    BubbleSort(number_of_parameters, player_x, card_numeric_value);
    score = PointCalc(player_x, number_of_parameters, flush_cards, straight_cards, straight_flush_cards, card_numeric_value);
    if (score == 5 || score == 9) {
        ArrayFiller(player_x, straight_cards);
    }
    return score;
}

//Recebe 7 cartas de um jogador faz print da melhor mão desse jogador e faz return do score
int SevenCards(int number_of_parameters, char player_cards[][3]){
    int card_numeric_value[number_of_parameters];
    int score;
    char flush_cards[8][3]; //Guarda as cartas que podem fazer parte de um flush
    char straight_cards[8][3]; //Guarda as cartas que podem fazer parte de um straight
    char straight_flush_cards[8][3]; //Guarda as cartas que podem fazer parte de um straight flush
    
    BubbleSort(number_of_parameters, player_cards, card_numeric_value);
    score = PointCalc(player_cards, number_of_parameters, flush_cards, straight_cards, straight_flush_cards, card_numeric_value);
        
    if (score >= MinScore()) { //Não interfere no modo -c. Apenas nos modos -dy
        if (PlayerID() != 0) { //Se o player id for maior que o score, faz-se print do ID do jogador seguido da sua mão
            WriteIntOutFile("%d ", PlayerID());
        }
        BestHandPrint(player_cards, number_of_parameters, flush_cards, straight_cards, straight_flush_cards, card_numeric_value, score);
    }
    
    return score;
}

//Recebe 9 cartas sendo que 2 pertencem a um jogador e 2 pertencem a outro, as restantes cartas estão na mesa
int NineCards (int number_of_cards, char player_cards[][3]){
    int i;
    int winner = 0;
    int score_player_1 = 0, score_player_2 = 0;
    char player_1[8][3];
    char player_2[8][3];
    
    //Este ciclo guarda as cartas exclusivas de cada jogador
    for (i = 0; i < 2; i++) {
        strcpy(player_1[i], player_cards[i]);
        strcpy(player_2[i], player_cards[i+2]);//Offset para que não seja necessário fazer 2 vezes o ciclo
    }
    
    //Guarda as cartas que estão na mesa na mão de ambos os jogadores
    for (i = 0 ; i < 5; i++) {
        strcpy(player_1[i+2], player_cards[i+4]);
        strcpy(player_2[i+2], player_cards[i+4]);
    }
    
    //Apenas necessário para poder usar algumas das funções auxiliares
    player_1[7][0] = '\0';
    player_2[7][0] = '\0';
    
    //Cálculo do score do Player 1
    score_player_1 = SevenCards(7, player_1);
    StatsDataPoints(score_player_1);
    WriteIntOutFile("%d ", score_player_1);
    
    //Cálculo do score do Player 2
    score_player_2 = SevenCards(7, player_2);
    StatsDataPoints(score_player_2);
    WriteIntOutFile("%d ", score_player_2);
    
    //Verifica qual dos jogadores ganha
    winner = WhoWins(score_player_1, score_player_2, 5, player_1, player_2);
    StatsDataWhoWins(winner);
    
    return winner;
}

//Recebe 10 cartas as 5 primeiras são do player 1 e as restantes são do player 2
int TenCards(int number_of_parameters, char arr[][3]) {
    int i;
    int winner = 0;
    int score_player_1 = 0, score_player_2 = 0;
    char player_1[6][3];
    char player_2[6][3];
    
    for (i = 0; i< 5; i++) {
        //Cartas do player 1 (primeiras 5)
        strcpy(player_1[i], arr[i]);
        //Cartas do player 2 (últimas 5)
        strcpy(player_2[i], arr[i+5]); //Offset de 5 posições para que só seja necessário fazer um ciclo com 5 repetições
    }
    //Apenas necessário para poder usar algumas das funções auxiliares
    player_1[5][0] = '\0';
    player_2[5][0] = '\0';
    
    //Cálculo do score do Player 1
    score_player_1 = FiveCards(5, player_1);
    StatsDataPoints(score_player_1);

    //Cálculo do score do Player 2
    score_player_2 = FiveCards(5, player_2);
    StatsDataPoints(score_player_2);

    //Verifica qual dos jogadores ganha
    winner = WhoWins(score_player_1, score_player_2, 5, player_1, player_2);
   StatsDataWhoWins(winner);
    
    return winner;
}


