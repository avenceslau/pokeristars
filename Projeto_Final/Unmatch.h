//
//  Unmatch.h
//  Projeto_Final
//
//  Created by André Venceslau, Alexandre Lopes on 06/04/2020.
//  Copyright © 2020 André Venceslau, Alexandre Lopes. All rights reserved.
//

#ifndef Unmatch_h
#define Unmatch_h

#include <stdio.h>
#include <stdbool.h>

#include "DxAux.h"

int WhoWins(int score_player_1, int score_player_2, int number_of_parameters_per_player, char player_1[][3], char player_2[][3]);
bool UnmatchDx(player_node * head);

#endif /* Unmatch_h */
