//
//  DeckMode.c
//  Projeto_Final
//
//  Created by André Venceslau, Alexandre Lopes on 28/04/2020.
//  Copyright © 2020 André Venceslau, Alexandre Lopes. All rights reserved.
//

#include <stdlib.h>
#include <string.h>

#include "Pointing_System.h"
#include "auxiliary_func.h"
#include "Command_Mode.h"
#include "Statistics.h"
#include "DxAux.h"
#include "Unmatch.h"

#define CHAR_IN_DECK 104


//Variável que representa um deck
typedef char deck[DECK_SIZE];

//Pointer para uma função do tipo int (int, char), serve para passar por argumento as funções Five,Seven,Nine,TenCards
typedef int (*func)(int number_of_parameters, char hand[][3]);

//Pointer para uma função do tipo void (deck, int *, void *), serve para que se possa passar por argumento as funções DEx, DInt
typedef void (*dy)(deck current_deck, int * ncards, void * special);

//adiciona um jogador à lista de jogadores. Recebe a head da lista e a tail
player_node * AddPlayer(player_node **head, player_node **tail){
    player_node * new;
    
    new = (player_node*) malloc(sizeof(player_node));
    if (*head == NULL) {
        *head = new;
        *tail = new;
        new->next = NULL;
        new->player_id = 1;
        Initializer(new);
    } else {
        (*tail)->next = new;
        new->next = NULL;
        new->previous = (*tail);
        new->player_id = (*tail)->player_id +1;
        Initializer(new);
        *tail = new;
    }
    return new;
}

//Recebe como parâmetros o baralho o número de cartas e a função associada ao nº de cartas
void BreakingDeck(char deck[], int ncards, func right_function){
    int i, x, counter;
    char hand[ncards][3];
    int max_hands = 52/ncards;
    int num_hands;
    int score;
    
    //Itera todas as cartas do deck e coloca as num vetor mão ncartas de cada vez
    for (i = 0, counter = 0, x = 0, num_hands = 0 ; i < CHAR_IN_DECK; i++) {
        if (x == 0) {
            hand[counter][x] = deck[i];
            x++;
        } else if (x == 1) {
            hand[counter][x] = deck[i];
            hand[counter][2] = '\0';
            x = 0;
            counter++;
        }
        //Quando forem colocadas ncartas na mão então é feita a validação de que as cartas nessa mão são válidas
        if (counter  == ncards) {
            //Se forem válidas então corre a função associada e imprime o resultado
            if (ncards == BetterCardChecker(ncards, hand)){
                if (CheckForRepCards(ncards, hand) != 0) {
                    StatsDataPoints(0);
                    if (ncards > 7) {
                        StatsDataWhoWins(0);
                        StatsDataPoints(0);
                    }
                    WriteIntOutFile("%d\n", -1);
                } else {
                    score = right_function(ncards, hand);
                    if (ncards < 9) {
                        StatsDataPoints(score);
                    }
                    WriteIntOutFile("%d\n", score);
                }
            } else {
                StatsDataPoints(0);
                if (ncards > 7) {
                    StatsDataWhoWins(0);
                    StatsDataPoints(0);
                }
                WriteIntOutFile("%d\n", -1);
            }
            counter = 0;
            num_hands++;
        }
        if (max_hands == num_hands) {
            break;
        }
    }
    WriteCharOutFile("%s", "\n");
}

//-d1, d2, d3, d4
//recebe como parâmetros a variável do tipo deck, o nº de cartas e um pointer para a função associada a esse nº de cartas
void DInt(deck current_deck, int *ncards, void * func_before_cast){
    func right_function;
    
    right_function = (func) func_before_cast; //typecast para o tipo de função a utilizar
    BreakingDeck(current_deck, *ncards, right_function);
}

//Distribuição das cartas pelos jogadores. Recebe uma variável do tipo deck, um pointer para a head dos jogadores, e um vetor para colocar as cartas na mesa
void DealCards(deck current_deck, player_node * player_head, char table[6][3]){
    int i, j;
    int k; //posição k do deck
    player_node * ptr_to_player;
    
    ptr_to_player = player_head;
    //São dadas a cada jogador as suas cartas exclusivas
    for (i = 0, k = 0 ; i < PLAYER_NUM ; i++) {
        DoIPlay(ptr_to_player);
        ptr_to_player->hand[2][0] = '\0';
        ptr_to_player->best_hand[5][0] = '\0';
        ptr_to_player->score = 0;
        ArrayCleaner(ptr_to_player->hand, ArraySize(ptr_to_player->hand));
        ArrayCleaner(ptr_to_player->best_hand, ArraySize(ptr_to_player->best_hand));
        if (ptr_to_player->cooldown == false) {
            for (j = 0; j < 2 ; j++) {
                ptr_to_player->hand[j][0] = current_deck[k];
                k++;
                ptr_to_player->hand[j][1] = current_deck[k];
                k++;
                ptr_to_player->hand[j][2] = '\0';
            }
        }
        ptr_to_player = ptr_to_player->next;
    }
    //São dadas as cartas para a mesa
    for (i = 0 ; i < 5 ; i++) {
        table[i][0] = current_deck[k];
        k++;
        table[i][1] = current_deck[k];
        k++;
        table[i][2] = '\0';
    }
}

//Função que determina que jogadores iram jogar. Recebe um pointer para a head dos jogadores, e o vetor com as cartas da mesa
void LetsPlay(player_node * player_head, char table[6][3]){
    char hand_tmp[8][3]; //Mão do jogador em causa durante a interação do ciclo abaixo
    player_node * ptr_to_player = NULL;
    hand_tmp[7][0] = '\0';
    
    ptr_to_player = player_head;
    //Para cada jogador serão colocadas numa mão temporária as cartas exclusivas de cada jogador e as cartas da mesa
    do {
        GetPlayerId(ptr_to_player->player_id);
        PutCardsToHand(hand_tmp, ptr_to_player->hand, table);
        //É verificado para cada jogador se se reunem as condições necessárias para jogar ou não jogar Ex.: se jogou mais de 2 vezes seguidas ou se fez fold pelo menos 4 vezes
        DoIPlay(ptr_to_player);
        //Verifica se o jogador é obrigado a jogar
        if (ptr_to_player->forced_play == true) {
            //Player Id o faz com que o jogador jogue sempre
            GetMinScore(0);
            ptr_to_player->score = SevenCards(7, hand_tmp);
            WriteIntOutFile("%d\n", ptr_to_player->score);
            //Guarda-se a melhor mão do jogador
            ptr_to_player->best_hand[5][0] = '\0';
            RegularCleanAndFill(ptr_to_player->best_hand, hand_tmp);
            ptr_to_player->consecutive_plays++;
            ptr_to_player->num_folds = 0;
        }
        //Verifica-se se o jogador não estiver em cooldown
        else if (ptr_to_player->cooldown == false) {
            GetMinScore(ptr_to_player->player_id);
            ptr_to_player->score = SevenCards(7, hand_tmp);
            //Se o score for maior do que o id então vai a jogo
            if (ptr_to_player->score >= ptr_to_player->player_id) {
                //guarda-se a melhor mão
                WriteIntOutFile("%d\n", ptr_to_player->score);
                RegularCleanAndFill(ptr_to_player->best_hand, hand_tmp);
                ptr_to_player->consecutive_plays++;
                ptr_to_player->num_folds = 0;
                //Se o score não for maior que o id então o jogador faz fold
            } else {
                ptr_to_player->score = 0;
                ptr_to_player->consecutive_plays = 0;
                ptr_to_player->num_folds++;
            }
        } else if (ptr_to_player->cooldown == true){
            CooldownReset(ptr_to_player);
        }
        ptr_to_player = ptr_to_player->next;
    } while (ptr_to_player != NULL);
    ArrayCleaner(hand_tmp, 7);
}

//-dx   Recebe como argumentos uma variável do tipo deck, um pointer para o número de jogos, e um pointer para o jogador
void DEx(deck current_deck, int *ngames, void * player_before_cast){
    player_node * ptr_to_player;
    char table[6][3];
    
    table[5][0] = '\0';
    ptr_to_player = (player_node *) player_before_cast;
    
    //Para todos os decks
    DealCards(current_deck, ptr_to_player, table);
    LetsPlay(ptr_to_player, table);
    if (UnmatchDx(ptr_to_player) == true) {
        (*ngames)++;
    }
}

//Para cada deck, coloca-o na variável do tipo deck, e chama a função correta, dependendo do ncards (di pode ser 1,2,3,4)
void AddDecks(FILE * file, bool di_mode, int ncards){
    int i = 0;
    int ch;
    void * pointer_to_cast = NULL;
    player_node * player_head = NULL;
    player_node * player_tail = NULL;
    dy what_mode = NULL;
    deck current_deck;
    
    if (di_mode == true) {
        what_mode = &DInt;
        if (ncards == 5) {pointer_to_cast = (void *) &FiveCards;}
        else if (ncards == 7) {pointer_to_cast = (void *) &SevenCards;}
        else if (ncards == 9) {pointer_to_cast = (void *) &NineCards;}
        else if (ncards == 10) {pointer_to_cast = (void *) &TenCards;}
    } else {
        what_mode = &DEx;
        for (i = 0; i < PLAYER_NUM ; i++) {
            AddPlayer(&player_head, &player_tail);
        }
        ncards = 0; //no modo -dx, a variável ncards funciona como ngames ( guarda o número de jogos)
        pointer_to_cast = (void *) player_head;
    }
    i = 0;
    do {
        ch = fgetc(file);
        if ((ch != ' ') && (ch != '\n') && (ch != EOF) && (ch != '\t')) {
            //Adiciona-se a carta ao baralho
            current_deck[i] = ch;
            i++;
            //se i for igual a 104 então o deck tem 52 cartas. Ao contar 52 cartas, chama-se a função DEx ou DInt. Depois fazemos reset do i, procedendo para o deck seguinte
            if (i == CHAR_IN_DECK) {
                i = 0;
                what_mode(current_deck, &ncards, pointer_to_cast); //what_mode é um pointer para a função correta ( -DEx ou -DInt)
            }
        }
    } while (ch != EOF);
    
    if (di_mode == true) {
        StatsCalculator(ncards);
    } else {
        StatsCalculatorDx(ncards);
        WriteCharOutFile("%s", "\n");
        PrintStatDx(player_head);
    }
}

//Seleciona -dx ou -di
void DModeSelection(char * arguments[]){
    FILE * file = NULL;
    int ncards = 0;
    bool di = NULL;
    
    //dependendo do 3º carater de -dy é selecionada qual a função necessária e é atribuído o número de cartas
    if (arguments[1][2] == '1') { ncards = 5; di = true;}
    else if (arguments[1][2] == '2') { ncards = 7; di = true;}
    else if (arguments[1][2] == '3') { ncards = 9; di = true;}
    else if (arguments[1][2] == '4') { ncards = 10; di = true;}
    else if (arguments[1][2] == 'x') { ncards = 0; di = false;}
    
    file = GetFile(arguments[2]);
    GenerateStats();
    AddDecks(file, di, ncards);
    fclose(file);
    CloseOutFile();
}
