//
//  DxAux.c
//  Projeto_Final
//
//  Created by André Venceslau, Alexandre Lopes on 07/05/2020.
//  Copyright © 2020 André Venceslau, Alexandre Lopes. All rights reserved.
//
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "DxAux.h"
#include "auxiliary_func.h"

unsigned int min_score = 0;
unsigned int current_id = 0;

int PlayerID(void){
    return current_id;
}

int MinScore(void){
    return min_score;
}

//Coloca na variável current_id o id do jogador atual
void GetPlayerId(int player_id){
     current_id = player_id;
}

//Define-se para o jogador x o seu score mínimo, para os casos em que é obrigado jogar considera-se player id 0
void GetMinScore(int player_id){
    min_score = player_id;
}


//Procura por cartas repetidas
int CheckForRepCards(int num_of_parameters, char cards[][3]){
    int ncards = 0;
    int i, j;
    
    if (num_of_parameters == 10) {
        for (i = 0 ; i < 4 ; i++) {
            for (j = i + 1 ; j < 5 ; j++) {
                // se houver repeticoes na primeira mao, sai com -1
                if (strcmp(cards[i], cards[j]) == 0) {
                    ncards++;
                }
                // se houver repeticoes na segunda mao, sai com -1
                if (strcmp(cards[i+5], cards[j+5]) == 0) {
                    ncards++;
                }
            }
        }
    }
    //Todos os outros casos
    else {
        //Verifica se existem cartas repetidas na mão
        for (i = 0 ; i < num_of_parameters - 1 ; i++) {
            for (j = i + 1 ; j < num_of_parameters ; j++) {
                if (strcmp(cards[i], cards[j]) == 0) {
                    ncards++;
                }
            }
        }
    }
    return ncards;
}

//Initializa todos os membros do jogador atual a 0
void Initializer(player_node * ptr){
    (ptr)->consecutive_plays = 0;
    (ptr)->num_folds = 0;
    (ptr)->score = 0;
    (ptr)->num_wins = 0;
    (ptr)->forced_play = false;
    (ptr)->cooldown = false;
}

//Faz reset das condições que levam a cooldown
void CooldownReset(player_node * ptr_to_player){
    ptr_to_player->cooldown = false;
    ptr_to_player->num_folds = 0;
    ptr_to_player->consecutive_plays = 0;
}

//Coloca as cartas na mão dos jogadores
void PutCardsToHand(char hand[7][3], char exclusive_player_cards[2][3], char table[5][3]){
    int i, position = 0;
    
    for (i = 0; i < 2; i++) {
        strcpy(hand[position], exclusive_player_cards[i]);
        position++;
    }
    for (i = 0; i < 5; i++) {
        strcpy(hand[position], table[i]);
        position++;
    }
}


//Faz reset dos scores do jogo atual
void ResetPlayerScores(bool who_points[PLAYER_NUM]){
    int i;
    for (i = 0 ; i < PLAYER_NUM ; i++) {
        who_points[i] = false;
    }
    
}

//Verifica se o jogador está em cooldown, obrigado a jogar, ou em caso normal
void DoIPlay(player_node * player_x){
    if (player_x->consecutive_plays == 2) {
        player_x->cooldown = true;
    } else if (player_x->num_folds == 4){
        player_x->forced_play = true;
    } else {
        player_x->cooldown = false;
        player_x->forced_play = false;
    }
}

//Limpa os vetores da melhor mão do jogador, tal como a sua mão completa (contém as cartas da mesa) e o seu score
void ResetRound(player_node * player_x){
    player_x->score  = 0;
    ArrayCleaner(player_x->best_hand, 5);
    ArrayCleaner(player_x->hand, 5);
}

