//
//  Statistics.c
//  Projeto_Final
//
//  Created by André Venceslau, Alexandre Lopes on 28/04/2020.
//  Copyright © 2020 André Venceslau, Alexandre Lopes. All rights reserved.
//

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include "auxiliary_func.h"
#include "Statistics.h"
#include "DxAux.h"


struct statistics{
    int points[11]; //diferentes pontuações Ex.: -1,1,2,3,...
    int who_wins[3]; //empate/quem ganhou respetivamente 0,1,2
} game_stats;


//Stats init a zero
void GenerateStats(){
    int i;
    for(i=0;i<10;i++){
        game_stats.points[i] = 0;
    }
    for (i = 0; i < 3 ; i++) {
        game_stats.who_wins[i] = 0;
    }
}


//Por cada jogo, incrementa o número total de vezes que cada mão é jogada
void StatsDataPoints(int score){
    game_stats.points[score]++ ;
}

//Por cada jogo, incrementa o número total de jogos ganhos do jogador 1,2 ou empate
void StatsDataWhoWins(int winner){
    game_stats.who_wins[winner]++;
}

//Calcula ratio de cada mão
void StatsCalculator(int ncards){
    int i,total_games=0;
    double ratio[11];
   
    //calcula número total de jogos
    for(i=0;i<11;i++){
        total_games+=game_stats.points[i];
    }
    for(i=0;i<11;i++){
        ratio[i] = (double) game_stats.points[i]/total_games;
    }
    
    if(ncards == 9 || ncards == 10){
        StatsPrintRatio(ratio, true);
        StatsPrintWinner();
    }
    else{
        StatsPrintRatio(ratio, true);
    }
    
}

//Cálcula o ratio de cada mão no caso de ser modo Dx
void StatsCalculatorDx(int ngames){
    int i;
    double ratio[11];
    for(i=0;i<11;i++){
        ratio[i] = (double) game_stats.points[i]/ngames;
    }
    StatsPrintRatio(ratio, false);
}

//Print do ratio
void StatsPrintRatio(double ratio[], bool di){
    int i;

    for(i=0;i<11;i++){
        if(i == 0){
            if (di == true) {
                WriteDoubleOutFile("-1 %.2E\n",ratio[i]);
            }
        }
        else{
            WriteIntOutFile("%d ", i);
            WriteDoubleOutFile("%.2E\n",ratio[i]);
        }
    }
}

//Print das vitórias por jogador
void StatsPrintWinner(){
    int i;
    
    WriteCharOutFile("%s", "\n");
    for(i=0;i<3;i++){
        WriteIntOutFile("%d ",i);
        WriteIntOutFile("%d\n",game_stats.who_wins[i]);
    }
}

//Calcula a pontuação somada aos vencedores no final da ronda, e soma aos pontos totais de cada jogador
void StatsDx(player_node * head, bool who_points[PLAYER_NUM], int num_winners){
    double points = (double) 1/num_winners;
    player_node * ptr_to_player = NULL;
    
    ptr_to_player = head;
    do {
        if (who_points[ptr_to_player->player_id-1] == true) {
            ptr_to_player->num_wins += points;
            ResetRound(ptr_to_player);
        }
        ptr_to_player = ptr_to_player->next;
    } while (ptr_to_player != NULL);
}

//Print do número de pontos de cada jogador por ordem
void PrintStatDx(player_node * head){
    int i = 0;
    int player_ids[PLAYER_NUM];
    double player_num_wins[PLAYER_NUM];
    player_node * ptr_to_player = NULL;
    ptr_to_player = head;
    
    do{
        player_ids[i] = ptr_to_player->player_id;
        player_num_wins[i] = ptr_to_player->num_wins;
        ptr_to_player = ptr_to_player->next;
        i++;
    } while(ptr_to_player != NULL);
    PlayerBubbleSort(player_ids, player_num_wins);
    for(i = PLAYER_NUM-1 ; i >= 0 ; i--){
        WriteIntOutFile("%d ",player_ids[i]);
        WriteDoubleOutFile("%.2lf\n",player_num_wins[i]);
    }
}


