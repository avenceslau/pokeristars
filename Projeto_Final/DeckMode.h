//
//  DeckMode.h
//  Projeto_Final
//
//  Created by André Venceslau, Alexandre Lopes on 28/04/2020.
//  Copyright © 2020 André Venceslau, Alexandre Lopes. All rights reserved.
//

#ifndef DeckMode_h
#define DeckMode_h

#include <stdio.h>
#include <stdbool.h>

#define DECK_SIZE 104
#define NUM_OF_CHAR 2
#define PLAYER_NUM 8

void DModeSelection(char * arguments[]);

#endif /* DeckMode_h */
